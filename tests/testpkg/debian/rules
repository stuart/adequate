#!/usr/bin/make -f

tmp = debian/tmp
at = adequate-test

.PHONY: clean
clean:
	dh_clean

.PHONY: build
build: build-stamp

build-stamp:
	mkdir -p $(tmp)
	# bin-or-sbin-binary-requires-usr-lib-library; library-not-found
	$(CC) lib.c -fPIC -shared -Wl,-soname,libadequate-lib.so.0 -o $(tmp)/libadequate-lib.so.0
	ln -sf libadequate-lib.so.0 $(tmp)/libadequate-lib.so
	$(CC) lib.c -fPIC -shared -Wl,-soname,libadequate-usrlib.so.0 -o $(tmp)/libadequate-usrlib.so.0
	ln -sf libadequate-usrlib.so.0 $(tmp)/libadequate-usrlib.so
	$(CC) prog.c -L$(tmp) -Wl,--no-as-needed -ladequate-lib -o $(tmp)/adequate-lib1
	cd $(tmp) && seq 2 5 | xargs -t -I {} ln -f adequate-lib1 adequate-lib{}
	$(CC) prog.c -L$(tmp) -Wl,--no-as-needed -ladequate-usrlib -o $(tmp)/adequate-usrlib1
	cd $(tmp) && seq 2 5 | xargs -t -I {} ln -f adequate-usrlib1 adequate-usrlib{}
	# incompatible-licenses
	$(CC) lib.c -fPIC -shared -Wl,-soname,libssl.so.1.0.0 -o $(tmp)/libssl.so
	$(CC) lib.c -fPIC -shared -Wl,-soname,libreadline.so.6 -o $(tmp)/libreadline.so
	$(CC) prog.c -L$(tmp) -Wl,--no-as-needed -lssl -lreadline -o $(tmp)/adequate-license-conflict
	$(CC) prog.c -L$(tmp) -Wl,--no-as-needed -lreadline -o $(tmp)/adequate-license-conflict-dep5
	# missing-version-information
	$(CC) -shared -Wl,--soname=lib$(at)-versionless.so.0 -Wl,--version-script=verscript-global lib.c -o $(tmp)/lib$(at)-versionless.so.0
	ln -sf lib$(at)-versionless.so.0 $(tmp)/lib$(at)-versionless.so
	$(CC) undef.c -L$(tmp) -o $(tmp)/$(at)-msvi -l$(at)-versionless
	$(CC) -shared -Wl,--soname=lib$(at)-versionless.so.0 lib.c -o $(tmp)/lib$(at)-versionless.so.0
	# symbol-size-mismatch
	$(CC) -shared -Wl,--soname=lib$(at)-symsize.so.0 lib.c -o $(tmp)/lib$(at)-symsize.so.0
	ln -sf lib$(at)-symsize.so.0 $(tmp)/lib$(at)-symsize.so
	$(CC) symsize.c -L$(tmp) -o $(tmp)/$(at)-symsize -l$(at)-symsize
	$(CC) -shared -Wl,--soname=lib$(at)-symsize.so.0 -DADEQUATE_SYMBOL_SIZE=42 lib.c -o $(tmp)/lib$(at)-symsize.so.0
	# undefined-symbol
	$(CC) -shared -Wl,--soname=lib$(at)-versioning.so.0 -Wl,--version-script=verscript-global lib.c -o $(tmp)/lib$(at)-versioning.so.0
	ln -sf lib$(at)-versioning.so.0 $(tmp)/lib$(at)-versioning.so
	$(CC) undef.c -fPIE -Wl,--unresolved-symbols,ignore-all -L$(tmp) -o $(tmp)/$(at)-us1
	$(CC) undef.c -L$(tmp) -o $(tmp)/$(at)-us2 -l$(at)-versioning
	$(CC) -shared -Wl,--soname=lib$(at)-versioning.so.0 -Wl,--version-script=verscript-local lib.c -o $(tmp)/lib$(at)-versioning.so.0
	# py-file-not-bytecompiled; pyshared-file-not-bytecompiled
	cp *.py debian/tmp/
	python3 -m compileall debian/tmp/
	touch $(@)

.PHONY: build-arch build-indep
build-arch build-indep:
	$(error $(@) is not implemented)

.PHONY: binary
binary:
	dh_install
	dh_installdocs -N adequate-testpkg-missing-copyright-file
	dh_installchangelogs
	dh_link
	dh_compress
	dh_fixperms
	dh_strip --no-automatic-dbgsym
	dh_makeshlibs
	dh_installdeb
	dh_gencontrol
	dh_md5sums
	dh_builddeb
	rm debian/adequate-testpkg-obsolete-conffile/etc/adequate/test-obsolete-conffile
	sed -i -e '/test-obsolete/d' debian/adequate-testpkg-obsolete-conffile/DEBIAN/conffiles
	cp -f debian/files debian/files.orig
	dh_gencontrol -p adequate-testpkg-obsolete-conffile -- -v2 -fdebian/files2
	cat debian/files2 >> debian/files
	rm -f debian/files2
	dh_md5sums -p adequate-testpkg-obsolete-conffile
	dh_builddeb -p adequate-testpkg-obsolete-conffile

.PHONY: binary-arch binary-indep
binary-arch binary-indep:
	$(error $(@) is not implemented)

# vim:ts=4 sts=4 sw=4 noet
